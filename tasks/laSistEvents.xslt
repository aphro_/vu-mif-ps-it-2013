<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
xmlns:bb="https://bitbucket.org/teraxas/vu-mif-ps-oop2-2012-lazsist">
    <xsl:template match="/">
        <html>
            <head>
                <title>Renginių sąrašas</title>
            </head>
            <body>
                <h1>Renginių sąrašas</h1>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="bb:LsEvent">
        <h2>
            <xsl:value-of select="bb:eventName"/>
        </h2>
            <p>
            <xsl:text>Renginio data/laikas: </xsl:text>
            <xsl:value-of select="bb:eventDate"/>
            <br/>
            <xsl:text>Renginio vieta: </xsl:text>
            <xsl:value-of select="bb:eventLocation"/>
            <br/>
            <xsl:text>Renginio tipas: </xsl:text>
            <xsl:value-of select="bb:eventType"/>
            <br/>
            <xsl:text>Bilieto kaina: </xsl:text>
            <xsl:value-of select="@ticketPrice"/>
            <br/>
            <xsl:text>Dalyvauja: </xsl:text>
            <br/>
            <xsl:apply-templates select="bb:teamsLs"/>
        </p>
    </xsl:template>
    
    <xsl:template match="bb:teamsLs">
            <xsl:value-of select="bb:rate"/>

        <xsl:for-each select="bb:team">
            <xsl:sort select="bb:name" data-type="text" order="ascending"/>
            <xsl:number value="position()" format="a) "/>
            <xsl:apply-templates select="bb:name"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="bb:rate"/>
            <xsl:if test="(bb:teamsLs/@favorite) = (bb:name)">
                <xsl:text> - favoritas</xsl:text>
            </xsl:if>
            <br/>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>
