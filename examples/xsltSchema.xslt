<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:template match="/">
        <body>
            <h1>Renginiai</h1>
            <xsl:apply-templates/>
        </body>
    </xsl:template>
    
    <xsl:template match="renginiai">
         <table border="1" cellpadding="10">
            <tr align="center">
                <th rowspan="2">Nr</th>
                <th rowspan="2">Renginys</th>
                <th rowspan="2">Čempionatas</th>
                <th colspan="2">Pirma komanda</th>
                <th colspan="2">Antra komanda</th>
                <th rowspan="2">Data</th>
                <th rowspan="2">Vieta</th>
                <th rowspan="2">Komanda su didele tikimybe laimėti</th>
            </tr>
            <tr align="center">
                <th>Pavadinimas</th>
                <th>Šalis</th>
                <th>Pavadinimas</th>
                <th>Šalis</th>
            </tr>
            <xsl:apply-templates select="renginys">
                <xsl:sort select="eventName" lang="lt"/>
            </xsl:apply-templates>
        </table>
    </xsl:template>
    
    <xsl:template match="renginys">      
            <tr align="center">
                <td><xsl:number value="position()" format="1."/></td>
                <td><xsl:apply-templates select="eventName"/></td>
                <td><xsl:value-of select="details/@renginiuGrupe"/></td>
                <xsl:for-each select="komandos/komanda">
                    <td><xsl:value-of select="pavadinimas"/></td>
                    <td><xsl:value-of select="salis"/></td>
                </xsl:for-each>                  
                <td><xsl:value-of select="data"/></td>
                <td><xsl:value-of select="details/@vieta"/></td>
                
             
                
                <xsl:apply-templates select="komandos"/>           
            </tr>
    </xsl:template>
    
    <xsl:template match="komanda">
        <xsl:if test="tikimybeLaimeti > 60">
            <td><xsl:value-of select="pavadinimas"/></td>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>